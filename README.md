# N5 Challenge
App micro front in React and module federation
## Main
this app run in port (9000)
```bash
yarn start or yarn start:live
```
## Movies 
this app run in port (9001)
```bash
yarn start or yarn start:live
```

## Search
this app run in port (9002)
```bash
yarn start or yarn start:live
```

## Question
### What is accessibility? How do you achieve it?
la accesibilidad es la condición que deben cumplir los entornos, procesos, objetos, herramientas y dispositivos, para ser comprensibles y utilizables por todas las personas, con la máxima autonomía posible
Para lograr la accesibilidad web hay que ser conscientes de que no todos los usuarios acceden a la Web con los mismos medios técnicos y que no todos los usuarios son capaces de percibir correctamente todos los contenidos publicados en la Web. 
para esto hay que cumplir pautas desarrolladas por W3C y el Gobierno de los eeuu.

### What is the difference between session storage, local storage, and cookies?
La diferencia es que el sesionStorage te va a guardar la informacion hasta que cierre la pestaña de l navegador, en localStorage la informacion queda disponbile hasta que se limpie cache y la cookie la informacion queda disponible por el tiempo que definas al momento de crearla y este ultimo la informacion guardada viaja automaticamente al backend donde la informacion puede ser leida facilmente.

### Explain the javascript event loop (also may explain how promises or async await work in js these are basically all the same question/answer)

### If you are getting too many API calls being made from your hooks, what can you do to prevent this?
no entendi la pregunta.

### How do you manage the global state? And why in that way?
el manageState es un estado a nivel global hoy en dia tenemos en react Context que te permite compartir el estado en toda tu app, tambien existen otras librerias populares como redux o mobx que te permite manejar el estado de tu app. el estado global te permite eliminar el props drealing y tambien tener tu estado de aplicacion en un solo lugar como unica fuente de verdad.

### What is progressive rendering?
es renderizar contenido html del lado del servidor.