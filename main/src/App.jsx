import React, { Suspense, useState } from "react";
import {  Route, Routes } from "react-router-dom";
import "./index.scss";
import './i18n/config';

import Header from "./Components/Header";
import Home from "./Components/Home";
import Loader from "./Components/Loader";
import NotFound from "./Components/NotFound";

const RemoteMoviesApp = React.lazy(() => import("movies/MoviesApp"));
const RemoteSearch = React.lazy(() => import("search/Search"));

import styled from "styled-components"

const Section = styled.section`
  padding: 1rem;
`;

const App = () => {
  const [ loading, setLoading ] = useState(false);
  return (
    <>
      <Section>
        <Header />  
      </Section>
      <Section id="search">
        <RemoteSearch />
      </Section>
      <Section>
      <Suspense fallback={<Loader />}>
        <Routes>
          {/* <Route path="/" element={<Home loading={loading} setLoading={setLoading}/>}/> */}
          <Route  path="/" element={<RemoteMoviesApp />}/>
          <Route path="*" element={<NotFound />}/>
        </Routes>
      </Suspense>
      </Section>
    </>
  )
};

export default App