import React, {FC} from "react"

interface FlagProps {
  country: string
  onClick: (flag: string) => void
}

const Flag:FC<FlagProps> = ({country, onClick}) => {
  return (
    <img
    loading="lazy"
    width="36"
    onClick={() => onClick(country)}
    src={`https://flagcdn.com/h48/${country.toLowerCase()}.png`}
    alt=""
  />
  )
}

export default Flag