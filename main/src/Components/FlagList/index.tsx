import React, { FC } from "react"
import { useTranslation } from "react-i18next"
import Flag from "../Flag"
import { ContainerFlags } from "./styled"

const FlagList: FC = () => {
  const { i18n } = useTranslation()

  const onHandleFlag = (flag: string) => {
    const lang = flag === 'us' ? 'en' : 'es'
    i18n.changeLanguage(lang)

  }
  return <ContainerFlags>
    {['es', 'us'].map(c => <Flag country={c} onClick={onHandleFlag} />) }
  </ContainerFlags>
}

export default FlagList
