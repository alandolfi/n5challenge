import styled from "styled-components"

export const ContainerFlags = styled.div`
  display: flex;
  flex-direction: row;
  gap: 10px;
  cursor: pointer;
  margin-right: 1rem;
`