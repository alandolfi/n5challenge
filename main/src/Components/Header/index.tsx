import React from "react"
import { HeaderContainer, Logo, Title} from "./styled"
import { useTranslation } from 'react-i18next';
import FlagList from "../FlagList";

const Header = () => {  
  const { t } = useTranslation('home')
  return (
    <HeaderContainer>
      <Logo />
      <Title>{t('title')}</Title>      
      <FlagList />
    </HeaderContainer>
  )
}

export default Header