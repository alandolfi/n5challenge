import React from "react"
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Loader from './Loader';
const RemoteProductCard = React.lazy(() => import("movies/ProductCard"));
const RemoteMoviesCard = React.lazy(() => import("movies/MoviesCard"));

export default ({loading, setLoading}) => {

    const [ products , setProducts ] = useState([]);
    useEffect(() => {
        if(!products.length){
            fetechProducts()
        }
    }, [products])

   async  function fetechProducts () {
        setLoading(true)
        const { data } = await  axios.get("https://fakestoreapi.com/products?limit=3")
        setProducts(data);
        setLoading(false)
    }

    if(loading) {
        return <Loader />
    }

    return <>
        <RemoteMoviesCard />
        {/* {products.map((p) => <RemoteProductCard  index={p.id} product={p}/>)} */}
    </>
                
}