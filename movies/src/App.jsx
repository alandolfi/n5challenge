import React from "react";
import {  Route, Routes } from "react-router-dom";

import "./index.scss";
import MoviesLists from "./Components/MoviesList";


const App = () => {
   return <div>
    <Routes>
      <Route path="/" element={<MoviesLists />}/> 
     </Routes>
   </div>
 
};

  export default App;
