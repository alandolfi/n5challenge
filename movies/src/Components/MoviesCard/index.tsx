import { FC } from "react"
import { Movies } from "src/model/movies"
import { Card, CardContent, CardImage } from "./styled"

interface MoviesCardProps {
  movie: Movies
}

const MoviesCard:FC<MoviesCardProps> = ({ movie }) => {
  return(
    <Card>
      <CardImage urlImage={movie.urlImage} alt={movie.title} />
      <CardContent>
        <p>{movie.title}</p>
      </CardContent>
    </Card>
  )
}

export default MoviesCard