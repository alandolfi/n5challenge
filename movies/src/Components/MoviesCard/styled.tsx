import { url } from "inspector"
import styled from "styled-components"

export const Card = styled.div`
  width: 270px;
  background: white;
  border-radius: 8px;
  border: 1px solid #DDD;
`

export const CardImage = styled.img<{ urlImage: string}>`
  background: url(${props=> props.urlImage});
  width: 270px;
  height: 130px;
  background-repeat: no-repeat;
  object-fit: cover;
  background-size: 100%;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
`

export const CardContent = styled.div`
  padding: 1rem;
`