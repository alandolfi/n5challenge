import axios from "axios";
import { FC, useEffect, useState } from "react"
import useRapidApi from "../../hooks/useRapidApi";
import MoviesCard from "../MoviesCard"
import { MoviesListContainer, LoadingSpinner } from "./styled"



const MoviesList:FC = () => {    
  const { movies, loading } = useRapidApi()
   
  if(loading) {
    return <LoadingSpinner loading={loading}/>
  }

  return <MoviesListContainer>
    { movies.map((p) => <MoviesCard movie={p} /> )}
  </MoviesListContainer>
}

export default MoviesList