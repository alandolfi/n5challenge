import styled, { keyframes }  from "styled-components"

export const MoviesListContainer = styled.div`
  display:flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: 10px;
`
const rotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
}`

export const LoadingSpinner = styled.div<{ loading: boolean}>`
  border: 4px solid rgba(255, 255, 255, 0.5);
  border-radius: 50%;
  border-top-color: white;
  opacity: ${({ loading }) => loading ? 1 : 0};
  position: absolute;
  left: 25%;
  right: 25%;
  top: 25%;
  bottom: 25%;
  margin: auto;
  width: 150px;
  height: 150px;
  transition: opacity 200ms;
  animation: ${rotate} 1s linear;
  animation-iteration-count: infinite;
  transition-delay: ${({ loading }) => loading ? '200ms' : '0ms'}`
