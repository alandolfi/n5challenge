// import Axios from 'axios'
// import useAxios, { configure } from "axios-hooks"
import React, { useEffect, useState } from "react"
import { Movies } from "src/model/movies"
import { transformToModelMovies } from '../utils/transformToModel'
import { mockHarry } from "../mocks/harry"
import { mockRick } from '../mocks/rick'

// const axios = Axios.create({
//   baseURL: 'https://watchmode.p.rapidapi.com/autocomplete-search/',
//   headers: {
//     'X-RapidAPI-Key': process.env.RAPID_API_KEY,
//     'X-RapidAPI-Host': 'imdb-search2.p.rapidapi.com'
//   }
// })

// configure({ axios })

interface useRapidReponse {
  movies: Movies[],
  loading: boolean,
  onSearchMovies: (search: string) => void
}

const useRapidApi = (): useRapidReponse => {
  const [loading, setLoading] = useState<boolean>(false)
  const [movies, setMovies] = useState<Movies[]>([])

  useEffect(() => {    
    window.addEventListener('searchMovies', handleEvent);
    return () => {
      document.removeEventListener('searchMovies', handleEvent);
    };
  },[])

  const handleEvent = (e) => {
    setLoading(true)
    const search = e.detail;
    setTimeout(() => {
      if(search.includes('harry')) {        
        setMovies(transformToModelMovies(mockHarry))
      }else if(search.includes('rick')) {
        setMovies(transformToModelMovies(mockRick))
      }
      setLoading(false)
    },3000)
  }

  // const [{ data, loading }, refetch] = useAxios(
  //   `?search_value=${search}&search_type=1`,
  //   {
  //     manual: true
  //   }
  // )

  // const onSearchMovies = async (searchMovie: string) => {
  //   // setSearch(searchMovie)
  // }

  return {
    movies,
    loading
  } as useRapidReponse
}

export default useRapidApi