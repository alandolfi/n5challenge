export const mockHarry = {
  "results": [
    {
      "name": "Harry Potter and the Deathly Hallows: Part 2",
      "relevance": 249.75,
      "type": "movie",
      "id": 1159475,
      "year": 2011,
      "result_type": "title",
      "tmdb_id": 12445,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159475_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Half-Blood Prince",
      "relevance": 249.75,
      "type": "movie",
      "id": 1159477,
      "year": 2009,
      "result_type": "title",
      "tmdb_id": 767,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159477_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Chamber of Secrets",
      "relevance": 249.5,
      "type": "movie",
      "id": 1159473,
      "year": 2002,
      "result_type": "title",
      "tmdb_id": 672,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159473_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Philosopher's Stone",
      "relevance": 249.5,
      "type": "movie",
      "id": 1159480,
      "year": 2001,
      "result_type": "title",
      "tmdb_id": 671,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159480_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Goblet of Fire",
      "relevance": 249.5,
      "type": "movie",
      "id": 1159476,
      "year": 2005,
      "result_type": "title",
      "tmdb_id": 674,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159476_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Order of the Phoenix",
      "relevance": 249.5,
      "type": "movie",
      "id": 1159478,
      "year": 2007,
      "result_type": "title",
      "tmdb_id": 675,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159478_poster_w185.jpg"
    },
    {
      "name": "Harry Potter",
      "relevance": 210.21,
      "type": "tv_series",
      "id": 3195932,
      "year": null,
      "result_type": "title",
      "tmdb_id": 224377,
      "tmdb_type": "tv",
      "image_url": "https://cdn.watchmode.com/posters/03195932_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Deathly Hallows: Part 1",
      "relevance": 199.8,
      "type": "movie",
      "id": 1159474,
      "year": 2010,
      "result_type": "title",
      "tmdb_id": 12444,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159474_poster_w185.jpg"
    },
    {
      "name": "Harry Potter and the Prisoner of Azkaban",
      "relevance": 199.6,
      "type": "movie",
      "id": 1159479,
      "year": 2004,
      "result_type": "title",
      "tmdb_id": 673,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01159479_poster_w185.jpg"
    },
    {
      "name": "Harry Potter 20th Anniversary: Return to Hogwarts",
      "relevance": 199.4,
      "type": "tv_special",
      "id": 543171,
      "year": 2022,
      "result_type": "title",
      "tmdb_id": 899082,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/0543171_poster_w185.jpg"
    },
  
  ]
}