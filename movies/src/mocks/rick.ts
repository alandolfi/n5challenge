export const mockRick = {
  "results": [
    {
      "name": "Rick and Morty",
      "relevance": 446.85,
      "type": "tv_series",
      "id": 3100109,
      "year": 2013,
      "result_type": "title",
      "tmdb_id": 60625,
      "tmdb_type": "tv",
      "image_url": "https://cdn.watchmode.com/posters/03100109_poster_w185.jpg"
    },
    {
      "name": "Rick and Morty: The Great Yokai Battle of Akihabara",
      "relevance": 226.5,
      "type": "movie",
      "id": 1736364,
      "year": 2021,
      "result_type": "title",
      "tmdb_id": 1164486,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01736364_poster_w185.jpg"
    },
    {
      "name": "Rick and Morty: Summer Meets God (Rick Meets Evil)",
      "relevance": 83.3,
      "type": "movie",
      "id": 1674468,
      "year": 2021,
      "result_type": "title",
      "tmdb_id": 858068,
      "tmdb_type": "movie",
      "image_url": "https://cdn.watchmode.com/posters/01674468_poster_w185.jpg"
    }   
  ]
}