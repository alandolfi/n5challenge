export interface Movies {
  title: string,
  urlImage: string,
  description: string,
  year: string,
  img_poster: string,
  imdb_url: string
} 