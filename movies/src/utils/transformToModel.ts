import { Movies } from "../model/movies"

export const transformToModelMovies = (data: any): Movies[] => {
  if(data) {
    return data.results.map((movie: any) => ({
      title: movie.name,
      urlImage: movie.image_url,
    }))
  }
  return []
}