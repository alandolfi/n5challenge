import React, { useState } from "react";
import ReactDOM from "react-dom";
import { Route, Routes } from "react-router-dom";

import "./index.scss";
import Filters from "./Components/filters";

const App = () => {
  return <div className="border-4  border-green-700">
  <Routes>
   <Route path="/" element={<Filters />}/>
 </Routes>
 </div>
};

export default App;

