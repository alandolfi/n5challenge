import React from "react"
import { ButtonsContainer, Button, ButtonHarry } from "./styled"
import { useTranslation } from "react-i18next"
const Search = () => {
  const { t } = useTranslation()

  const onSearch = (search:string) => {
    console.log('search', search)
    window.dispatchEvent(
      new CustomEvent('searchMovies', {
        'detail': search 
      })
    );
  }
  return (
    <ButtonsContainer>
      <Button onClick={() => onSearch('harry potter')}> {t('button.harry')} </Button>
      <Button onClick={() => onSearch('rick and morty')}> {t('button.ricky')} </Button>
    </ButtonsContainer>
  )
}

export default Search