import styled from "styled-components"


export const Button = styled.button`
  padding: 0.5rem;
  border: none;
  color: #000;
  font-weight: 400;
`

export const ButtonHarry = styled(Button)`
  background: red;
`

export const ButtonsContainer = styled.div`
  display:flex;
  flex-direction: row;
  gap: 10px;
`